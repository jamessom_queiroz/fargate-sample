FROM ruby:2.5.3-alpine

# Setup environment variables that will be available to the instance
ENV APP_HOME /produciton

# Installation of dependencies
RUN apk update && apk upgrade &&\
  apk --no-cache add build-base tzdata nodejs ca-certificates yarn mysql-dev &&\
  rm -rf /var/cache/apk/*

# Create a directory for our application
# and set it as the working directory
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# Add our Gemfile
# and install gems

ADD Gemfile* $APP_HOME/
RUN bundle install

# Copy over our application code
ADD . $APP_HOME

ENTRYPOINT [ "sh", "./bin/start" ]